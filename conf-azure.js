/**
 * Created by johan on 08/01/2016.
 */

var fs = require('fs');


module.exports.getAccount = function getAccount() {

    return JSON.parse(fs.readFileSync('./config.json', {encoding: 'utf8'}));
};