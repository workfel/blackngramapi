(function () {
    'use strict';

    angular.module('blackNGram.timeLine')
        .service('TimeLineService', TimeLineService)


    TimeLineService.$inject = ['$http', 'URL'];


    function TimeLineService($http, URL) {

        this.getAllPictures = function (cb) {

            $http.get(URL.getAllPictures).then(function success(response) {
                cb(null, response.data);
            }, function error(response) {
                cb(response.data);
            });

        };


    }

})();
