(function () {
    'use strict';

    angular.module('blackNGram.timeLine', [
            'ngRoute',
            'blackNGram.url',
            'blackNGram.user'])

        .config(Config)
        .controller('TimeLineCtrl', TimeLineCtrl);


    Config.$inject = ['$routeProvider'];


    function Config($routeProvider) {
        $routeProvider.when('/time-line', {
            templateUrl: 'app/time-line/timeline.html',
            controller: 'TimeLineCtrl as vm'
        });
    }


    TimeLineCtrl.$inject = ['$location', 'TimeLineService'];

    function TimeLineCtrl($location, TimeLineService) {

        var vm = this;


        function _init() {
            _getAllImages();
        }

        //Method
        vm.reloadPictures = _reloadPictures;


        function _reloadPictures() {
            _getAllImages();
        }

        function _getAllImages() {
            TimeLineService.getAllPictures(function (err, pictures) {
                if (err) {
                    vm.error = err;
                } else {
                    vm.pictures = pictures;
                }
            })
        }


        _init();
    }


})();
