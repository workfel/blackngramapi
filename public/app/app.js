(function () {
    'use strict';

// Declare app level module which depends on views, and components
    angular.module('blackNGram', [
        'ngRoute',
        'blackNGram.login',
        'blackNGram.profile',
        'blackNGram.timeLine',
        'blackNGram.url',
        'blackNGram.user',
        'myApp.version',
        'FBAngular'
    ]).config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/login'});
    }]);
})();

