/**
 * Created by johan on 07/01/2016.
 */
(function () {
    'use strict';


    angular.module('blackNGram.user', [])
        .service('UserService', UserService);


    UserService.$inject = [];

    function UserService() {

        var _userConnected = null;

        this.setUserConnected = function setUserConnected(user) {
            _userConnected = user;
        };


        this.getUserConnected = function getUserConnected() {
            return _userConnected;

        }
    }
})();