/**
 * Created by johan on 07/01/2016.
 */
(function () {
    'use strict';

    var baseUrl = "";

    angular.module('blackNGram.url', [])
        .constant('URL', {
            login: baseUrl + '/api/user',
            uploadPicture: baseUrl + '/api/upload/',
            getAllPictures: baseUrl + '/api/picture/',
            picturesUser: baseUrl + '/api/picture/',
            deletePicture: baseUrl + '/api/picture/'
        })
})();