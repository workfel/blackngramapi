(function () {
    'use strict';

    angular.module('blackNGram.login', [
            'ngRoute',
            'blackNGram.url',
            'blackNGram.user'])

        .config(Config)
        .controller('LoginCtrl', LoginCtrl);


    Config.$inject = ['$routeProvider'];


    function Config($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'app/login/login.html',
            controller: 'LoginCtrl as vm'
        });
    }


    LoginCtrl.$inject = ['$http', 'URL', '$location', 'UserService'];

    function LoginCtrl($http, URL, $location, UserService) {

        var vm = this;


        //Method

        vm.onLogin = _onLogin;


        function _onLogin() {

            if (!vm.pseudo) {
                vm.error = "Rentrer un pseudo";
                return;
            }

            $http.get(URL.login + '/' + vm.pseudo).then(function success(response) {

                console.log(response);

                if (response.data.entries.length === 0) {
                    vm.error = "Mauvais pseudo";
                } else {
                    UserService.setUserConnected(response.data.entries[0]);
                    $location.path('/profile');
                }

            }, function error(response) {
                alert(response.data);
            });

        }
    }


})();
