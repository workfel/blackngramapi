(function () {
    'use strict';

    angular.module('blackNGram.profile')
        .service('ProfileService', ProfileService)


    ProfileService.$inject = ['$http', 'URL'];


    function ProfileService($http, URL) {


        //TODO : wait api
        var _pictures = [
            {
                id: 0,
                img: 'http://lorempixel.com/400/200/sports/1',
                imgNoColor: 'http://lorempixel.com/400/200/sports/1'
            },
            {
                id: 1,
                img: 'http://lorempixel.com/400/200/sports/2',
                imgNoColor: 'http://lorempixel.com/400/200/sports/2'
            },
            {
                id: 2,
                img: 'http://lorempixel.com/400/200/sports/3',
                imgNoColor: null
            }
        ];

        this.getUserPictures = function (user, cb) {

            $http.get(URL.picturesUser + user.Pseudo._).then(function success(response) {
                cb(null, response.data);
            }, function error(response) {
                cb(response.data);
            });
        };


        this.deletePicture = function (pseudo, picture, cb) {
            $http({
                method: 'DELETE',
                url: URL.deletePicture + pseudo,
                data: {picture: picture}
            }).then(function success(response) {
                cb(null, response.data);
            }, function error(response) {
                cb(response.data);
            });

            //cb(null, picture);
        }
    }

})();
