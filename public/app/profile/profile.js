(function () {
    'use strict';

    angular.module('blackNGram.profile', [
            'ngRoute',
            'blackNGram.url',
            'ngFileUpload',
            'blackNGram.user'])

        .config(Config)
        .controller('ProfileCtrl', ProfileCtrl);


    Config.$inject = ['$routeProvider'];


    function Config($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'app/profile/profile.html',
            controller: 'ProfileCtrl as vm'
        });
    }


    ProfileCtrl.$inject = ['$http', 'URL', '$location', 'UserService', 'ProfileService', 'Upload'];

    function ProfileCtrl($http, URL, $location, UserService, ProfileService, Upload) {

        var vm = this;

        vm.user = UserService.getUserConnected();

        console.log(vm.user);

        if (!vm.user) {
            return $location.path('/');
        }


        function _init() {
            _getUserImages();
        }

        //Method
        vm.reloadPictures = _reloadPictures;
        vm.deletePicture = _deletePicture;
        vm.upload = _upload;


        function _upload(file) {

            console.log(file);


            Upload.upload({
                url: URL.uploadPicture,
                fields: {
                    'fileName': vm.fileName,
                    'user': vm.user
                },
                file: file
            }).then(function (resp) {
                _reloadPictures();
            }, function (resp) {
                vm.error = 'Error status: ' + resp.status;
                console.log('Error status: ' + resp.status);
            });
        };


        function _deletePicture(picture) {

            ProfileService.deletePicture(vm.user.Pseudo._, picture, function (err, response) {

                if (err) {
                    vm.error = err;
                } else {
                    var index = vm.pictures.indexOf(picture);
                    if (index != -1)
                        vm.pictures.splice(index, 1);
                }
            });
        }


        function _reloadPictures() {
            _getUserImages();
        }

        function _getUserImages() {
            ProfileService.getUserPictures(vm.user, function (err, pictures) {
                if (err) {
                    vm.error = err;
                } else {
                    vm.pictures = pictures;
                }
            })
        }


        _init();
    }


})();
