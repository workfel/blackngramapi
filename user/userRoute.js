/**
 * Created by johan on 08/01/2016.
 */
var express = require('express');
var router = express.Router();
var controller = require('./user');


router.get('/', function (req, res) {


    //promise for the fun
    controller.findAll(function (err, result) {
        if (err) {
            return res.status(404).json(error);
        } else {
            return res.json(result);
        }

    });

});

router.get('/:pseudo', function (req, res) {

    var pseudo = req.params['pseudo'];
    if (pseudo) {
        controller.findByPseudo(pseudo, function (err, result) {
            if (err)
                res.status(404).json(err);
            else
                res.json(result);
        })
    } else {
        res.status(400).json({error: 'Le pseudo doit exister'});
    }
});


router.post('/', function (req, res) {

    if (!req.body.pseudo)
        res.status(400).json({msg: "Le pseudo doit être fourni"});


    controller.create(req.body.pseudo, function (err, result) {
        if (err)
            res.status(400).json(err);
        else
            res.json(result);
    });
});


module.exports = router;