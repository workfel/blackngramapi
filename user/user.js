/**
 * Created by johan on 08/01/2016.
 */
var azure = require('azure-storage');
var entGen = azure.TableUtilities.entityGenerator;
var uuid = require('node-uuid');
var Promise = require('promise');


var confAzure = require('../conf-azure').getAccount();

var tableService = azure.createTableService(confAzure.accountName, confAzure.accountKey);


var mTable = 'Users';

tableService.createTableIfNotExists(mTable, function (error, result, response) {
    if (!error) {
        // result contains true if created; false if already exists
        console.log(error);
    }
});


function _createUser(pseudo, cb) {

    var entity = {
        Pseudo: entGen.String(pseudo),
        PartitionKey: entGen.String(confAzure.partitionKey),
        RowKey: entGen.String(uuid.v1())
    };

    tableService.insertEntity(mTable, entity, function (error, result, response) {
        if (error) {
            // result contains the ETag for the new entity
            console.log('Error in create user' + JSON.stringify(error));
            return cb(error);
        }

        console.log(result);
        return cb(null, result);
    });
}

function _findAll(cb) {

    var query = new azure.TableQuery()
        .where('PartitionKey eq ?', confAzure.partitionKey);

    tableService.queryEntities(mTable, query, null, function (error, result, response) {
        if (error) {
            // result contains the ETag for the new entity
            console.log('Error in getting user list' + JSON.stringify(error));
            return cb(error);
        }

        console.log(result);
        return cb(null, result);
    });

}

function _findByPseudo(pseudo, cb) {
    var query = new azure.TableQuery()
        .where('PartitionKey eq ? and Pseudo eq ?', confAzure.partitionKey, pseudo);


    tableService.queryEntities(mTable, query, null, function (error, result, response) {
        if (error) {
            // result contains the ETag for the new entity
            console.log('Error in getting user by pseudo' + JSON.stringify(error));
            return cb(error);
        }


        return cb(null, result);
    });

}

function _addPicture(user, image, cb) {
    var pictures = [];


    if (user.Pictures) {

        if (typeof  user.Pictures._ === "string") {
            pictures = JSON.parse(user.Pictures._);
        } else {

            pictures = user.Pictures._;
        }
    }

    pictures.push({
        image: {
            blob: image.blob,
            url: image.url
        },
        imageBW: null
    });

    var updateEntity = {
        PartitionKey: entGen.String(user.PartitionKey._),
        RowKey: entGen.String(user.RowKey._),
        Pictures: entGen.String(JSON.stringify(pictures))
    };

    tableService.mergeEntity(mTable, updateEntity, function (error, result, response) {
        if (error) {
            console.log('Error in merge user' + JSON.stringify(error));
            return cb(error);
        }

        console.log(result);
        return cb(null, result);
    });
}


function _updateUser(user, cb) {

    tableService.updateEntity(mTable, user, function (error, result, response) {
        if (error) {
            console.log('Error in update user' + JSON.stringify(error));
            return cb(error);
        }

        return cb(null, result);
    });
}


module.exports = {
    create: _createUser,
    findAll: _findAll,
    findByPseudo: _findByPseudo,
    addPicture: _addPicture,
    updateUser: _updateUser
};