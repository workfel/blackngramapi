/**
 * Created by johan on 10/01/2016.
 */

var azure = require('azure-storage');



var confAzure = require('./conf-azure').getAccount();

var queueBw = azure.createQueueService(confAzure.accountName, confAzure.accountKey);
var queueThumb = azure.createQueueService(confAzure.accountName, confAzure.accountKey);

var queueBwName = 'blackandwhiterequest';
var queueThumbName = 'thumbnailrequest';

queueBw.createQueueIfNotExists(queueBwName, function (error, result, response) {
    if (error) {
        console.log(error);
    }
});

queueThumb.createQueueIfNotExists(queueThumbName, function (error, result, response) {
    if (error) {
        console.log(error);
    }
});


function _triggerQueueBlackAndWhite(blobInformation) {


    queueBw.createMessage(queueBwName, JSON.stringify(blobInformation), function (error, result, response) {
        if (error) {
            console.log(error);
        }
    });
}

function _triggerQueueThumbnail(blobInformation) {
    queueThumb.createMessage(queueThumbName, JSON.stringify(blobInformation), function (error, result, response) {
        if (error) {
            console.log(error);
        }
    });
}

module.exports = {
    triggerQueueBlackAndWhite: _triggerQueueBlackAndWhite,
    triggerQueueThumbnail: _triggerQueueThumbnail
}