# BlackNGramFrontEnd



## Installation

```bash
npm install
```

Place into public folder and run

```bash
bower install
```


### Run the Application


```
node app.js
```

Now browse to the app at `http://localhost:3000/`.