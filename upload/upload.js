/**
 * Created by johan on 08/01/2016.
 */
var fs = require('fs');
var azure = require('azure-storage');
var confAzure = require('../conf-azure').getAccount();
var blobService = azure.createBlobService(confAzure.accountName, confAzure.accountKey);


var mBlobContainer = 'pictures';


var userController = require('../user/user');
var queueServices = require('../queueServices');

blobService.createContainerIfNotExists(mBlobContainer, {
    publicAccessLevel: 'blob'
}, function (error, result, response) {
    if (!error) {
        console.log(result);
        // if result = true, container was created.
        // if result = false, container already existed.
    }
});


function _upload(user, fileName, pathFile, cb) {


    var blobname = user.Pseudo._ + "/" + fileName;

    blobService.createBlockBlobFromLocalFile(mBlobContainer, blobname, pathFile, function (error, result, response) {
        if (error) {
            console.log('Error in upload blob file' + JSON.stringify(error));
            return cb(error);
        }

        //Remove local File
        fs.unlinkSync(pathFile);


        var url = "http://" + confAzure.accountName + ".blob.core.windows.net" + '/' + mBlobContainer + '/' + blobname;

        var image = {
            blob: result,
            url: url
        };


        queueServices.triggerQueueBlackAndWhite({
            BlobUri: url,
            blackAndWhite: false
        });

        queueServices.triggerQueueThumbnail({
            BlobUri: url,
            blackAndWhite: false
        });


        userController.addPicture(user, image, function (err, updated) {

            if (err) {
                console.log('Error add Picture to TableStorage' + JSON.stringify(err));
                return cb(err);
            }

            return cb(null, updated);
        });


    });

}


module.exports = {
    upload: _upload
};