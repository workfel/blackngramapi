/**
 * Created by johan on 08/01/2016.
 */
var express = require('express');
var fs = require('fs');
var router = express.Router();

var controller = require('./upload');

var multer = require('multer');
var upload = multer({dest: 'uploads/'});

router.post('/', upload.single('file'), function (req, res) {

    //console.log('---------Upload----------------');
    //console.log(req.body); // form fields
    //console.log(req.file); // form files

    if(!req.file){
        return res.send(400);
    }

    var oldPath = req.file.destination + req.file.filename;
    var newPath = req.file.destination + req.file.filename + ".png";

    fs.renameSync(oldPath, newPath);

    //console.log('---------Upload----------------');

    var file = req.file;
    file.filename = req.file.filename + ".png";
    file.path = req.file.path + ".png";


    //res.json(file);
    controller.upload(req.body.user, req.body.fileName, file.path, function (err, result) {
        if (err) {
            return res.status(500).json(err);
        } else {
            return res.json(result);
        }

    })

});


module.exports = router;