/**
 * Created by johan on 09/01/2016.
 */
var express = require('express');
var router = express.Router();
var controller = require('./picture');


router.get('/', function (req, res) {


    //promise for the fun
    controller.findAll(function (err, result) {
        if (err) {
            return res.status(404).json(error);
        } else {
            return res.json(result);
        }
    });

});

router.get('/:pseudo', function (req, res) {

    var pseudo = req.params['pseudo'];
    if (pseudo) {
        controller.findPicturesOfUser(pseudo, function (err, result) {
            if (err)
                res.status(404).json(err);
            else
                res.json(result);
        })
    } else {
        res.status(400).json({error: 'Le pseudo doit exister'});
    }
});


router.get('/bw', function (req, res) {

    var url = req.params['url'];
    var blobName = req.params['blobName'];
    if (url && blobName) {
        controller.addBlackAndWhitePicture(blobName, url, function (err, result) {
            if (err)
                res.status(404).json(err);
            else
                res.json(result);
        })
    } else {
        res.status(400).json({error: 'Url or BlobName is missing'});
    }
});


router.get('/thumbnail', function (req, res) {

    var url = req.params['url'];
    var blobName = req.params['blobName'];
    var bw = req.params['bw'];
    if (url && blobName) {
        controller.addThumbnailPicture(blobName, url, bw, function (err, result) {
            if (err)
                res.status(404).json(err);
            else
                res.json(result);
        })
    } else {
        res.status(400).json({error: 'Url or BlobName is missing'});
    }
});


router.delete('/:pseudo', function (req, res) {
    var pseudo = req.params['pseudo'];

    if (!pseudo)
        res.status(400).json({msg: "Le pseudo doit être fourni"});


    controller.deletePictureOfUser(pseudo, req.body.picture, function (err, result) {
        if (err)
            res.status(400).json(err);
        else
            res.json(result);
    });
});


module.exports = router;