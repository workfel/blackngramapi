/**
 * Created by johan on 08/01/2016.
 */
var azure = require('azure-storage');

var userController = require('../user/user');

var queueServices = require('../queueServices');

function _findPicturesOfUser(pseudo, cb) {
    userController.findByPseudo(pseudo, function (err, result) {

        if (err) {
            cb(err);
        } else {
            if (result.entries.length > 0) {
                var pictures = [];

                if (!result.entries[0].Pictures)
                    return cb(null, []);

                pictures = JSON.parse(result.entries[0].Pictures._);


                cb(null, pictures);

            } else {
                cb(null, {error: 'User not found'});
            }
        }
    });

}


function _findAll(cb) {

    userController.findAll(function (err, result) {

        if (err) {
            return cb(err);
        } else {

            var users = result.entries;

            var pictures = [];


            users.forEach(function (user) {
                if (user.Pictures) {
                    if (user.Pictures._) {
                        var userPictures = JSON.parse(user.Pictures._);

                        userPictures.forEach(function (picture) {
                            pictures.push({
                                user: user,
                                picture: picture
                            });
                        });
                    }
                }
            });


            cb(null, pictures);

        }


    });

}

function _deletePictureOfUser(pseudo, picture, cb) {
    userController.findByPseudo(pseudo, function (err, result) {

        if (err) {
            cb(err);
        } else {
            if (result.entries.length > 0) {
                var pictures = [];

                if (!result.entries[0].Pictures)
                    return cb(null, []);


                pictures = JSON.parse(result.entries[0].Pictures._);

                var indexPicture = pictures.indexOf(picture);

                if (indexPicture != -1) {
                    pictures.slice(indexPicture, 1);
                }

                var user = result.entries[0];

                user.Pictures = JSON.stringify(pictures);

                userController.updateUser(user, function (err, userUpdate) {
                    if (err) {
                        return cb(err);
                    }

                    return cb(null, userUpdate);
                });

            } else {
                cb(null, {error: 'User not found'});
            }
        }
    });
}

function _addBlackAndWhitePicture(blobName, url, cb) {


    queueServices.triggerQueueThumbnail({
        BlobUri: url,
        blackAndWhite: true
    });


    var psuedo = blobName.split('/')[0];

    userController.findByPseudo(psuedo, function (err, result) {
        if (err) {
            cb(err);
        } else {
            if (!result.entries[0].Pictures)
                return cb(null, []);


            var userPictures = JSON.parse(user.Pictures._);

            userPictures.forEach(function (picture) {

                if (picture.image.blob.blob == blobName) {
                    picture.imageBW = {
                        url: url
                    };
                }
            });

            user.Pictures = JSON.stringify(userPictures);

            userController.updateUser(user, cb);
        }


    });

}


function _addThumbnailPicture(blobName, url, bw, cb) {
    var psuedo = blobName.split('/')[0];

    userController.findByPseudo(psuedo, function (err, result) {
        if (err) {
            cb(err);
        } else {
            if (!result.entries[0].Pictures)
                return cb(null, []);


            var userPictures = JSON.parse(user.Pictures._);

            userPictures.forEach(function (picture) {
                if (picture.image.blob.blob == blobName) {
                    if (bw) {
                        picture.imageBW = {
                            thumbnailUrl: url
                        };
                    }
                } else {
                    picture.image = {
                        thumbnailUrl: url
                    };
                }
            });

            user.Pictures = JSON.stringify(userPictures);

            userController.updateUser(user, cb);

        }


    });


}


module.exports = {
    findAll: _findAll,
    findPicturesOfUser: _findPicturesOfUser,
    addBlackAndWhitePicture: _addBlackAndWhitePicture,
    addThumbnailPicture: _addThumbnailPicture,
    deletePictureOfUser: _deletePictureOfUser
};