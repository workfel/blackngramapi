/**
 * Created by johan on 08/01/2016.
 */
var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var config = require('./config');


var app = express();


//Routers
var user = require('./user/userRoute');
var upload = require('./upload/uploadRoute');
var picture = require('./picture/pictureRoute');


app.use(bodyParser.urlencoded({'extended': 'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());


app.use(express.static(__dirname + '/public'));


//
app.use('/api/user', user);
app.use('/api/upload', upload);
app.use('/api/picture', picture);


var server = app.listen(config.port, function () {
    console.log("App listening on port " + config.port);
});